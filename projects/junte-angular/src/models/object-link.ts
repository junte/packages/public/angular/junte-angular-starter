export interface ObjectLink {
  id: number;
  presentation: string;
}
