export interface PagingResults<T> {
  count: number;
  results: T[];
}
