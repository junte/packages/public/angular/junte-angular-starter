import { normalize } from '@angular-devkit/core';
import { camelize, classify, dasherize, underscore } from '@angular-devkit/core/src/utils/strings';
import { apply, branchAndMerge, chain, filter, mergeWith, move, Rule, template, url } from '@angular-devkit/schematics';
import { SchemaOptions } from './schema';

const stringUtils = {dasherize, classify, underscore, camelize};

function filterTemplates(options: SchemaOptions): Rule {
  if (!options.service) {
    return filter(path => !path.match(/\.service\.ts$/) && !path.match(/-item\.ts$/) && !path.match(/\.bak$/));
  }
  return filter(path => !path.match(/\.bak$/));
}

export default function (options: SchemaOptions): Rule {
  options.path = options.path ? normalize(options.path) : options.path;

  let source: any;
  if (!!options.service) {
    options.name = options.service;
    source = apply(url('./files/service'), [
      filterTemplates(options),
      template({...stringUtils, ...options}),
      move('src/services')
    ]);
  } else if (!!options.model) {
    options.name = options.model;
    source = apply(url('./files/model'), [
      filterTemplates(options),
      template({...stringUtils, ...options}),
      move('src/models')
    ]);
  } else if (!!options.mock) {
    let key = 'helpers';

    switch (options.mock) {
      case 'h':
        key = 'helpers';
        break;
      case 'o':
        key = 'objects';
        break;
    }

    source = apply(url(`./files/mock/${key}`), [
      filterTemplates(options),
      template({...stringUtils, ...options}),
      move(`src/mocks/${key}`)
    ]);
  }


  return chain([branchAndMerge(chain([mergeWith(source)]))]);

}
