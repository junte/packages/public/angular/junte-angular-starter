import { InjectionToken } from '@angular/core';
import { PagingResults } from 'models/paging-results';
import { Observable } from 'rxjs';

export interface I<%= classify(name) %>Service {

}

export let <%= underscore(name) %>_service = new InjectionToken('I<%= classify(name) %>Service');
