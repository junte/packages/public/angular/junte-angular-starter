import { Injectable } from '@angular/core';
import { HttpService } from 'services/http.service';
import { Observable } from 'rxjs';
import { I<%= classify(name) %>Service } from 'services/<%= dasherize(name) %>/<%= dasherize(name) %>.interface';
import { PagingResults } from 'models/paging-results';

@Injectable()
export class <%= classify(name) %>Service implements I<%= classify(name) %>Service {

  constructor(private http: HttpService) {
  }

}
