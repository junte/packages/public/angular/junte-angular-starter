import {Signal} from '../../../projects/junte-angular/src/models/signal';

export class AnimalChangedSignal implements Signal {
  sender: any;
}
