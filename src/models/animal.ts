import {Field, Name} from 'serialize-ts';

export class Animal {

  @Field()
  @Name('animal_type')
  type: string;

  @Field()
  name: string;

  toString() {
    return `${this.type} - ${this.name}`;
  }
}
