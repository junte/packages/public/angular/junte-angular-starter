import {NgModule} from '@angular/core';
import {PreloadAllModules, RouterModule, Routes} from '@angular/router';
import {AuthorizationGuard} from '../../guards/authorization.guard';

const routes: Routes = [
  {
    path: 'config',
    loadChildren: () => import('components/config/config.module').then(m => m.ConfigModule)
  },
  {
    path: 'signin',
    loadChildren: () => import('components/signin/signin.module').then(m => m.SigninModule)
  },
  {
    path: 'changelog',
    canActivate: [AuthorizationGuard],
    loadChildren: () => import('components/changelog/changelog.module').then(m => m.ChangelogModule)
  },
  {
    path: 'lazy',
    loadChildren: () => import('components/lazy/lazy.module').then(m => m.LazyModule)
  },
  {
    path: 'me',
    loadChildren: () => import('components/me/me.module').then(m => m.MeModule)
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    preloadingStrategy: PreloadAllModules,
    paramsInheritanceStrategy: 'always'
  })],
  exports: [RouterModule]
})

export class AppRoutingModule {}
