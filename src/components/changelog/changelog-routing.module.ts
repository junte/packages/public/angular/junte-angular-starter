import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ChangelogComponent } from "./changelog.component";
import {Angular7Component} from "./angular7/angular7.component";

const routes: Routes = [{
    path: '',
    component: ChangelogComponent,
    children: [
      {
        path: '',
        redirectTo: 'angular7'
      },
      {
        path: 'angular7',
        component: Angular7Component
      },
    ]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ChangelogRoutingModule { }
