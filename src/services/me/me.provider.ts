import { AppConfig } from 'app-config';
import { me_service } from 'services/me/me.interface';
import { MeService } from 'services/me/me.service';
import { MeMockService } from 'services/me/me.service.mock';
import { HttpService } from '../../../projects/junte-angular/src/services/http.service';
import { HttpMockService } from '../../../projects/junte-angular/src/services/http-mock.service';

export function MeServiceFactory(httpService: HttpService, httpMockService: HttpMockService, config: AppConfig) {
  return config.useMocks ?
    new MeMockService(httpMockService) :
    new MeService(httpService);
}

export let MeServiceProvider = {
  provide: me_service,
  useFactory: MeServiceFactory,
  deps: [HttpService, HttpMockService, AppConfig]
};
