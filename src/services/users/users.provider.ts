import { UsersService } from './users.service';
import { HttpService } from '../../../projects/junte-angular/src/services/http.service';
import { HttpMockService } from '../../../projects/junte-angular/src/services/http-mock.service';
import { AppConfig } from '../../app-config';
import { UsersMockService } from './users.service.mock';
import { users_service } from 'services/users/users.interface';

export function usersServiceFactory(httpService: HttpService, httpMockService: HttpMockService, config: AppConfig) {
  return config.useMocks ?
    new UsersMockService(httpMockService) :
    new UsersService(httpService);
}

export let usersServiceProvider = {
  provide: users_service,
  useFactory: usersServiceFactory,
  deps: [HttpService, HttpMockService, AppConfig]
};
