import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { IUsersService } from 'services/users/users.interface';
import { HttpService } from '../../../projects/junte-angular/src/services/http.service';
import { Authorization } from '../../../projects/junte-angular/src/models/authorization';
import { LoginCredentials } from '../../models/login-credentials';

@Injectable()
export class UsersService implements IUsersService {

  constructor(private http: HttpService) {
  }

  login(credentials: LoginCredentials): Observable<Authorization> {
    return this.http.post<Authorization>('login', credentials);
  }
}
