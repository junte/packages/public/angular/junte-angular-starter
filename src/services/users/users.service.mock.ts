import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { IUsersService } from 'services/users/users.interface';
import { HttpMockService } from '../../../projects/junte-angular/src/services/http-mock.service';
import { Authorization } from '../../../projects/junte-angular/src/models/authorization';
import { LoginCredentials } from 'models/login-credentials';

@Injectable()
export class UsersMockService implements IUsersService {

  constructor(private http: HttpMockService) {
  }

  login(credentials: LoginCredentials): Observable<Authorization> {
    return this.http.get(`users/login.json`);
  }
}
