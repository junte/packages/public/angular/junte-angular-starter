import {test_service} from 'services/test/test.interface';
import {TestService} from 'services/test/test.service';
import {TestMockService} from 'services/test/test.service.mock';
import {HttpService} from '../../../projects/junte-angular/src/services/http.service';
import {HttpMockService} from '../../../projects/junte-angular/src/services/http-mock.service';
import {Config} from '../../../projects/junte-angular/src/config';
import {AppConfig} from '../../app-config';

export function TestServiceFactory(httpService: HttpService,
                                   httpMockService: HttpMockService,
                                   config: AppConfig) {
  return config.useMocks ?
    new TestMockService(httpMockService) :
    new TestService(httpService);
}

export let TestServiceProvider = {
  provide: test_service,
  useFactory: TestServiceFactory,
  deps: [HttpService, HttpMockService, Config]
};
