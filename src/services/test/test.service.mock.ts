import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {ITestService} from 'services/test/test.interface';
import {HttpMockService} from '../../../projects/junte-angular/src/services/http-mock.service';
import {Animal} from '../../models/animal';
import {map} from 'rxjs/operators';
import {deserialize} from 'serialize-ts';

@Injectable()
export class TestMockService implements ITestService {

  constructor(private http: HttpMockService) {
  }

  get(): Observable<Animal> {
    return this.http.get('test/get.json')
      .pipe(map(x => deserialize(x, Animal)));
  }
}
