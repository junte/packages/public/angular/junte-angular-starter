import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {ITestService} from 'services/test/test.interface';
import {HttpService} from '../../../projects/junte-angular/src/services/http.service';
import {map} from 'rxjs/operators';
import {deserialize} from 'serialize-ts';
import {Animal} from '../../models/animal';

@Injectable()
export class TestService implements ITestService {

  constructor(private http: HttpService) {
  }

  get(): Observable<Animal> {
    return this.http.get<Animal>(`test/2`)
      .pipe(map(x => deserialize(x, Animal)));
  }
}
