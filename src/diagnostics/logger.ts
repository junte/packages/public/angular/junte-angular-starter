import {LoggingLevel} from './loggin-level';

export class Logger {

  constructor(public name: string, public level: LoggingLevel = LoggingLevel.None) {
    if (name.charAt(0) === '@') {
      this.name += '|' + Math.floor(Math.random() * 1000).toString();
    }
  }

  debug(message: any, object: Object = null) {
    if (this.level === LoggingLevel.Debug
      || this.level === LoggingLevel.Info
      || this.level === LoggingLevel.Warning
      || this.level === LoggingLevel.Error) {
      console.log(`${this.name}: ${message}`);
      if (object) {
        console.log(object);
      }
    }
  }

  info(message: any) {
    if (this.level === LoggingLevel.Info
      || this.level === LoggingLevel.Warning
      || this.level === LoggingLevel.Error) {
      console.info(`${this.name}: ${message}`);
    }
  }

  warn(message: any) {
    if (this.level === LoggingLevel.Warning
      || this.level === LoggingLevel.Error) {
      console.warn(`${this.name}: ${message}`);
    }
  }

  error(message: any) {
    if (this.level === LoggingLevel.Error) {
      console.error(`${this.name}: ${message}`);
    }
  }

}
