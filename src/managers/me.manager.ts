import { Inject, Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { AppConfig } from 'app-config';
import { Me } from 'models/me';
import { BehaviorSubject } from 'rxjs';
import { distinctUntilChanged, map } from 'rxjs/operators';
import { IMeService, me_service } from 'services/me/me.interface';

const HEARTBEAT_INTERVAL = 30000;

@Injectable()
export class MeManager {

  private _heartbeat: any;

  user$: BehaviorSubject<Me> = new BehaviorSubject<Me>(null);

  set user(user: Me) {
    if (JSON.stringify(this.user) !== JSON.stringify(user)) {
      this.user$.next(user);
    }
  }

  get user(): Me {
    return this.user$.getValue();
  }

  logged$: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(!!this.user);

  set logged(logged: boolean) {
    if (this.logged !== logged) {
      this.logged$.next(logged);
    }
  }

  get logged() {
    return this.logged$.getValue();
  }

  constructor(@Inject(me_service) private meService: IMeService,
              private config: AppConfig,
              private router: Router) {

    this.config.authorization$.subscribe(token => {
      clearInterval(this._heartbeat);
      if (!!token) {
        this.meService.getMe().subscribe(user => this.user = user, () => this.user = null);
        this.ping();
      } else {
        this.user = null;
        this.router.navigate(['/signin']);
      }
    });

    this.user$.pipe(map(user => !!user), distinctUntilChanged())
      .subscribe(logged => this.logged = logged);
  }

  private ping() {
    clearTimeout(this._heartbeat);
    this.meService.heartbeat().subscribe(() =>
      this._heartbeat = setTimeout(() => this.ping(), HEARTBEAT_INTERVAL));
  }

  can(permission: string) {
    return this.user && this.user.permissions.includes(permission);
  }
}
